FROM openjdk:8-jre-alpine

# folders
RUN ["mkdir", "-p", "/opt/app"]
WORKDIR /opt/app

# ports
EXPOSE 8761

# entrypoint
COPY scripts/entrypoint.sh entrypoint.sh
ENTRYPOINT [ "sh", "entrypoint.sh" ]

# app executable
ARG JAR_FILE
ADD target/${JAR_FILE} app.jar

